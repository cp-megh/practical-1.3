package com.example.practical13;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.practical13.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding mainBinding;
    private int mScore1;
    private int mScore2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);

        mainBinding.decreaseTeam1.setOnClickListener(view1 -> {
            if (mScore1 > 0) {
                mScore1--;
                mainBinding.txtScoreCounter1.setText(String.valueOf(mScore1));
            } else {
                mScore1 = 0;
                mainBinding.txtScoreCounter1.setText(String.valueOf(0));
            }
        });

        mainBinding.increaseTeam1.setOnClickListener(view1 -> {
            mScore1++;
            mainBinding.txtScoreCounter1.setText(String.valueOf(mScore1));
        });

        mainBinding.decreaseTeam2.setOnClickListener(view1 -> {
            if (mScore2 > 0) {
                mScore2--;
                mainBinding.txtScoreCounter2.setText(String.valueOf(mScore2));
            } else {
                mScore2 = 0;
                mainBinding.txtScoreCounter2.setText(String.valueOf(0));
            }
        });

        mainBinding.increaseTeam2.setOnClickListener(view1 -> {
            mScore2++;
            mainBinding.txtScoreCounter2.setText(String.valueOf(mScore2));
        });

    }
}